<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/add_user',
    function()
    {
        /** @var \PDO $pdo */
        $pdo = DB::connection()->getPdo();

        $statement = $pdo->prepare(
            'INSERT INTO users (name, email, password, created_at, updated_at) VALUES (:name, :email, :passw, :created, :updated)'
        );

        $statement->execute(
            [
                ':name'    => 'name',
                'email'    => \sprintf('%s@email.com', \uniqid(microtime(\true), true)),
                ':passw'   => 'somePasswordHash',
                ':created' => \date('c'),
                ':updated' => \date('c')
            ]
        );
    }
);
