#!/usr/bin/env bash

#composer
cd /var/www \
   && composer install --no-scripts --no-suggest \
   && composer dump-autoload \
   && composer clear-cache

# clear var
rm -rf /var/www/storage

mkdir -p /var/www/storage/framework/cache \
   && mkdir -p /var/www/storage/framework/views \
   && mkdir -p /var/www/storage/framework/sessions \
   && mkdir -p /var/www/storage/logs \
   && chown -R www-data:www-data /var/www/storage

cd /var/www \
   && php artisan migrate:fresh

# roadrunner
/usr/local/bin/roadrunner/roadrunner-1.2.4-linux-amd64/rr serve -d -v -c /var/www/.rr.yaml
